module.exports.generateResponse = (code, data, headers) => {
  const defaultHeaders = {
    'Content-Type': 'application/json'
  };

  const response = {
    statusCode: code || 500,
    body: JSON.stringify(data) || 'Internal Server Error',
    headers: headers || defaultHeaders
  };

  return response;
};
