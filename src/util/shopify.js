const {GraphQLClient} = require('graphql-request');

// Initialize Client
const client = new GraphQLClient(process.env.SHOPIFY_URL, {
  headers: {
    'X-Shopify-Access-Token': process.env.SHOPIFY_TOKEN,
    'Content-Type': 'application/json'
  }
});

module.exports.getProductByHandle = (handle) => {
  return Promise.resolve().then(() => {
    const query = `{
            productByHandle(handle: "${handle}") {
                id
            }
        }`;

    return client.request(query);
  }).catch((err) => {
    throw new Error(err);
  });
};
