var S3 = require('aws-sdk/clients/s3');
var s3 = new S3();

module.exports.upload = (bucket, key, bufferData, contentType) => {
  return new Promise(function (resolve, reject) {
    var params = {
      Bucket: bucket,
      Key: key,
      Body: bufferData,
      ContentType: contentType
    };

    s3.putObject(params, function (err) {
      if (err) {
        console.error(`Error uploading ${bucket}:${key}`);
        reject(err);
      } else {
        console.info(`Successfully uploaded ${bucket}:${key}`);
        resolve(params);
      }
    });
  });
};
