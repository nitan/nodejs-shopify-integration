class APIError extends Error {
  constructor (
    HTTPCode = 500,
    message = 'Internal Server Error'
  ) {
    super(message);
    Error.captureStackTrace(this);

    this.message = message;
    this.status = HTTPCode;
  }
}

module.exports = {
  APIError
};
