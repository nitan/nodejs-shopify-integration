'use strict';

const uuidv1 = require('uuid/v1');
const _ = require('lodash');

const s3 = require('../util/s3');
const shopify = require('../util/shopify');
const { APIError } = require('../util/error');
const { generateResponse } = require('../util/gateway');

module.exports.getProduct = (event, context, callback) => {
  return Promise.resolve().then(() => {
    console.info('Received event:', JSON.stringify(event, null, 2));

    const productHandle = _.get(event, 'pathParameters.id', null);

    if (!productHandle) {
      throw new APIError(400, 'No ID specified');
    }

    return shopify.getProductByHandle(productHandle);
  }).then((data) => {
    const product = _.get(data, 'productByHandle', null);
    if (!product) {
      throw new APIError(404, 'Product Not Found');
    }

    const bucket = process.env.S3_BUCKET_NAME;
    const key = `${uuidv1()}.json`;

    return s3.upload(bucket, key, JSON.stringify(data), 'application/json');
  }).then((data) => {
    const fileLocation = `https://s3-${process.env.S3_REGION}.amazonaws.com/${data.Bucket}/${data.Key}`;
    const message = `Product found. Details saved in ${fileLocation}`;
    const response = generateResponse(200, {
      message: message,
      body: JSON.parse(data.Body)
    });

    callback(null, response);
  }).catch((err) => {
    if (err instanceof APIError) {
      const response = generateResponse(err.status, {
        message: err.message
      });

      callback(null, response);
    }

    callback(err);
  });
};
