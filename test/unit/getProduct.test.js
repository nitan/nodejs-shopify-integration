/* global describe it afterEach */

const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');

const { getProduct } = require('../../src/getProduct/handler');
const s3 = require('../../src/util/s3');
const shopify = require('../../src/util/shopify');

const sandbox = sinon.createSandbox();

describe('GetProduct Handler', () => {
  describe('#getProduct', () => {
    const event = {
      pathParameters: {
        id: 1
      }
    };

    afterEach(() => {
      sandbox.restore();
    });

    it('should return 200 if product is successfuly retrieved', (done) => {
      const s3Bucket = 'testBucket';
      const s3Key = 'testKey';
      const s3Body = { data: 'test' };
      const s3Region = undefined;
      const fileLocation = `https://s3-${s3Region}.amazonaws.com/${s3Bucket}/${s3Key}`;
      const expectedMessage = {
        message: `Product found. Details saved in ${fileLocation}`,
        body: s3Body
      };
      const expectedHeaders = { 'Content-Type': 'application/json' };

      sandbox.stub(s3, 'upload').returns({
        Bucket: s3Bucket,
        Key: s3Key,
        Body: JSON.stringify(s3Body),
        ContentType: 'application/json'
      });

      sandbox.stub(shopify, 'getProductByHandle').returns({
        productByHandle: true
      });

      getProduct(event, null, (err, data) => {
        if (err) console.error(err);

        expect(data).to.be.an('object');
        expect(data.statusCode).to.equal(200);
        expect(data.body).to.equal(JSON.stringify(expectedMessage));
        expect(data.headers).to.deep.equal(expectedHeaders);
        done();
      });
    });

    it('should return 404 if product is not found', (done) => {
      const expectedMessage = {
        message: 'Product Not Found'
      };
      const expectedHeaders = { 'Content-Type': 'application/json' };

      sandbox.stub(shopify, 'getProductByHandle').returns({
        productByHandle: null
      });

      getProduct(event, null, (err, data) => {
        if (err) console.error(err);

        expect(data).to.be.an('object');
        expect(data.statusCode).to.equal(404);
        expect(data.body).to.equal(JSON.stringify(expectedMessage));
        expect(data.headers).to.deep.equal(expectedHeaders);
        done();
      });
    });

    it('should return 400 if id is not found', (done) => {
      const invalidEvent = { pathParameters: {} };
      const expectedMessage = {
        message: 'No ID specified'
      };
      const expectedHeaders = { 'Content-Type': 'application/json' };

      getProduct(invalidEvent, null, (err, data) => {
        if (err) console.error(err);

        expect(data).to.be.an('object');
        expect(data.statusCode).to.equal(400);
        expect(data.body).to.equal(JSON.stringify(expectedMessage));
        expect(data.headers).to.deep.equal(expectedHeaders);
        done();
      });
    });
  });
});
