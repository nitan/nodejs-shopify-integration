/* global describe it */

const chai = require('chai');
const expect = chai.expect;

const gateway = require('../../src/util/gateway');

describe('Gateway', () => {
  describe('#generateResponse', () => {
    it('should return an object with status code, body, and headers', () => {
      const response = gateway.generateResponse(500, {data: 'test'}, {header: 'test'});

      expect(response).to.be.an('object');
      expect(response).to.have.property('statusCode');
      expect(response).to.have.property('body');
      expect(response).to.have.property('headers');
    });

    it('should return default status code 500 if not provided', () => {
      const response = gateway.generateResponse(null);
      const defaultStatusCode = 500;

      expect(response).to.have.property('statusCode');
      expect(response.statusCode).to.equal(defaultStatusCode);
    });

    it('should return default message if not provided', () => {
      const response = gateway.generateResponse(null);
      const defaultMessage = 'Internal Server Error';

      expect(response).to.have.property('body');
      expect(response.body).to.equal(defaultMessage);
    });

    it('should stringify the message', () => {
      const data = { data: 'test' };
      const response = gateway.generateResponse(null, data);

      expect(response).to.have.property('body');
      expect(response.body).to.equal(JSON.stringify(data));
    });

    it('should return default header if not provided', () => {
      const response = gateway.generateResponse(500, {data: 'test'});
      const defaultHeader = {
        'Content-Type': 'application/json'
      };

      expect(response).to.have.property('headers');
      expect(response.headers).to.deep.equal(defaultHeader);
    });
  });
});
