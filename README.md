# Node.js shopify integration 

A serverless application which makes a GraphQL call to the Shopify UI and saves the response body to an S3 Bucket. 

Deployed in AWS Lambda triggered by AWS API Gateway.


Productiondeployment can be accessed here:<br />
https://rv1ju4t0ph.execute-api.ap-southeast-1.amazonaws.com/prod/comments/{id}

## Features
- Integration with Shopify GraphQL API
- Save response to S3 bucket
- Error handling
- CI/CD ready

## Prerequisites
 - [Node v6.10+](https://nodejs.org/en/download/current/)
 - [Serverless](https://github.com/serverless/serverless)
 - [Shopify Account](https://help.shopify.com/api/graphql-admin-api/getting-started)
 - [AWS Account](https://aws.amazon.com/resources/create-account/)

## Getting Started
Clone the repo:

```bash
git clone https://gitlab.com/nitan/nodejs-shopify-integration.git
```



Install dependencies:
```bash
npm install
```
Set environment variables:
```bash
export SHOPIFY_URL=<url string>;
export SHOPIFY_TOKEN=<token string>
```

Lastly, <br />
Set up [provider credentials](https://github.com/serverless/serverless/blob/master/docs/providers/aws/guide/credentials.md)


## Running the code
Based on [serverless documentation](https://github.com/serverless/serverless)<br/><br />

Use this when you have made changes to your Functions, Events or Resources in serverless.yml or you simply want to deploy all changes within your Service at the same time.
```
serverless deploy 
```



Use this to quickly upload and overwrite your AWS Lambda code on AWS, allowing you to develop faster.
```
serverless deploy -f getProduct
```


Invokes the AWS Lambda Function on AWS and returns logs. <br />

```
serverless invoke -f getProduct --path src/getProduct/sample.json -l
```
`sample.json` holds a sample request event from API Gateway. Feel free to change to suit your case.

## Running the tests
```
npm run test
```


## Linter
To run linter:

```
npm run eslint
```

To fix errors automatically:

```
npm run eslint:fix
```

## Deployment to Production
Code is automatically deployed to AWS upon merge to master.
<br />ENV variables are set in Gitlab
<br />For steps on deployment, refer to `.gitlab-ci.yml`

## Future Improvements
- Support rollback of deployment
